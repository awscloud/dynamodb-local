# DynamoDB Local setup


* [Setting Up DynamoDB Local (Downloadable Version)](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)



Examples:


    java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -inMemory -port 8000
    
    
    java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -dbPath db -port 8000


